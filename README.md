# Documentation 

<http://python-future.org/compatible_idioms.html>

# Stratégie à suivre pour passer de python 2.x à python 3.x

- avoir des tests de validation du code 
- passer l'ensemble des tests en python 2
- mettre -3 en ligne de commande pour activer les warnings et passer à nouveau les tests
- passer le traducteur 2to3 sur l'aborescence du code. fixer le reste des problèmes à la main jusqu'à ce que les tests passent


